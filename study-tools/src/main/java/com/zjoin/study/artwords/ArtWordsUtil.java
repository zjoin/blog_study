package com.zjoin.study.artwords;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.LayeredWordCloud;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.bg.CircleBackground;
import com.kennycason.kumo.bg.PixelBoundryBackground;
import com.kennycason.kumo.bg.RectangleBackground;
import com.kennycason.kumo.font.KumoFont;
import com.kennycason.kumo.font.scale.SqrtFontScalar;
import com.kennycason.kumo.nlp.FrequencyAnalyzer;
import com.kennycason.kumo.nlp.normalize.Normalizer;
import com.kennycason.kumo.nlp.tokenizers.ChineseWordTokenizer;
import com.kennycason.kumo.palette.ColorPalette;
import com.kennycason.kumo.palette.LinearGradientColorPalette;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;


/**
 * 类名 ArtWordsUtil
 * 描述
 *
 * @Author yangxw
 * @Date 2020/4/8 12:10
 * @Version 1.0
 **/
public class ArtWordsUtil {
    
    public static void main(String[] args) throws IOException {
        circular();
    }
    
    public static void png() throws IOException {
        //建立词频分析器，设置词频，以及词语最短长度，此处的参数配置视情况而定即可
        FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
        frequencyAnalyzer.setWordFrequenciesToReturn(600);
        frequencyAnalyzer.setMinWordLength(3);
        
        //引入中文解析器
        frequencyAnalyzer.setWordTokenizer(new ChineseWordTokenizer());
        //指定文本文件路径，生成词频集合
        String[] books = {"Spring实战", "Spring源码深度解析", "SpringBoot实战",
                "SpringBoot2精髓", "一步一步学SpringBoot2", "Spring微服务实战",
                "Head First Java", "Java并发编程实战", "深入理解Java 虚拟机",
                "Head First Design", "effective java", "J2EE development without EJB",
                "TCP/IP卷一", " 计算机网络：自顶向下", "图解HTTP和图解TCP/IP",
                "计算机网络", "深入理解计算机系统", "现代操作系统", "Thinking in Java",
                "Linux内核设计与实现", "Unix网络编程", "数据结构与算法",
                "算法导论", "数据结构与算法（Java版）", "算法图解，啊哈算法",
                "剑指offer", "LeetCode", " Java编程思想", "深入理解java同步关键字", "Scala从入门到精通", "Java核心内容讲解",
                "Java核心技术卷一", "深入理解JVM虚拟机", "Java并发编程实战",
                " Java并发编程艺术", "Java性能调优指南", "Netty权威指南",
                "深入JavaWeb技术内幕", "How Tomcat Works", "Tomcat架构解析",
                "Spring实战", "Spring源码深度解析", "Spring MVC学习指南",
                "Maven实战", "sql必知必会", "深入浅出MySQL",
                "Spring cloud微服务实战", "SpringBoot与Docker微服务实战", "深入理解SpringBoot与微服务架构"
        };
        List<String> words = Arrays.asList(books);
        
        
        final List<WordFrequency> wordFrequencyList = frequencyAnalyzer.load(words);
        //.load("E:\\ciyun/wordcloud.txt");
        
        //设置图片分辨率
        Dimension dimension = new Dimension(600, 600);
        //此处的设置采用内置常量即可，生成词云对象
        WordCloud wordCloud = new WordCloud(dimension, CollisionMode.PIXEL_PERFECT);
        //设置边界及字体
        wordCloud.setPadding(2);
        Font font = new Font("STSong-Light", 2, 20);
        //设置词云显示的三种颜色，越靠前设置表示词频越高的词语的颜色
        wordCloud.setColorPalette(new LinearGradientColorPalette(Color.RED, Color.BLUE, Color.GREEN, 30, 30));
        wordCloud.setKumoFont(new KumoFont(font));
        //设置背景色
        wordCloud.setBackgroundColor(new Color(255, 255, 255));
        //设置背景图片
        //设置背景图层为圆形
        wordCloud.setBackground(new PixelBoundryBackground(FileUtil.getInputStream(new File("D:\\wechat/test4.png"))));
//        wordCloud.setBackground(new CircleBackground(255));
        wordCloud.setFontScalar(new SqrtFontScalar(12, 45));
        //生成词云
        wordCloud.build(wordFrequencyList);
        wordCloud.writeToFile("D:\\wechat/wechat6.png");
    }
    
    
    public static void circular() throws IOException {
        final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
        frequencyAnalyzer.setWordFrequenciesToReturn(600);
        frequencyAnalyzer.setMinWordLength(2);
        frequencyAnalyzer.setMaxWordLength(10);
        frequencyAnalyzer.setWordTokenizer(new ChineseWordTokenizer());
        frequencyAnalyzer.setCharacterEncoding("utf-8");
        
        
        String[] books = {"Spring实战", "Spring源码深度解析", "SpringBoot实战",
                "SpringBoot2精髓", "一步一步学SpringBoot2", "Spring微服务实战",
                "Head First Java", "Java并发编程实战", "深入理解Java 虚拟机",
                "Head First Design", "effective java", "J2EE development without EJB",
                "TCP/IP卷一", " 计算机网络：自顶向下", "图解HTTP和图解TCP/IP",
                "计算机网络", "深入理解计算机系统", "现代操作系统", "Thinking in Java",
                "Linux内核设计与实现", "Unix网络编程", "数据结构与算法", "大厂面试技巧100招",
                "算法导论", "数据结构与算法（Java版）", "算法图解·算法",
                "剑指offer", "LeetCode", " Java编程思想", "深入理解java同步关键字", "Scala从入门到精通", "Java核心内容讲解",
                "Java核心技术卷一", "深入理解JVM虚拟机", "Java并发编程实战", "java高并发编程", "java网络精讲", "疯狂Java每日一课",
                " Java并发编程艺术", "Java性能调优指南", "Netty权威指南",
                "深入JavaWeb技术内幕", "How Tomcat Works", "Tomcat架构解析",
                "Spring实战", "Spring源码深度解析", "Spring MVC学习指南",
                "Maven实战", "sql必知必会", "深入浅出MySQL",
                "网络是怎样连接的", "网络密码技术", "重构——改善既有代码的设计", "精通正则表达式", "禅与摩托车维修艺术",
                "C语言的设计", "计算机程序设计艺术", "编程人生", "UNIX环境高级编程", "编码：隐匿在计算机软硬件背后的语言",
                "计算机程序的构造和解释", "编程珠玑", "C++Primer中文版", "别闹了，费曼先生", "人件集:人性化的软件开发",
                "程序员修炼之道", "C程序设计语言", "深入浅出设计模式", "哥德尔、艾舍尔、巴赫书：集异璧之大成", "代码整洁之道",
                "Spring cloud微服务实战", "SpringBoot与Docker微服务实战", "深入理解SpringBoot与微服务架构"
        };
        List<String> words = Arrays.asList(books);
        final List<WordFrequency> wordFrequencies = frequencyAnalyzer.load("D:\\wechat/word.txt");
//        BufferedImage image = ImgUtil.read("D:\\wechat/tiger.png");
        final Dimension dimension = new Dimension(640, 400);
        final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.PIXEL_PERFECT);
        wordCloud.setPadding(2);
        wordCloud.setBackground(new RectangleBackground(dimension));
        
//        wordCloud.setBackground(new PixelBoundryBackground(ImgUtil.toStream(image,ImgUtil.IMAGE_TYPE_PNG)));
//        wordCloud.setColorPalette(new LinearGradientColorPalette(Color.RED, Color.BLUE, Color.GREEN, 30, 30));
        wordCloud.setColorPalette(new ColorPalette(new Color(0x009688), new Color(0xFFB800), new Color(0xFF5722), new Color(0x1E9FFF), new Color(0xEE00EE), new Color(0x68228B), new Color(0xfefefe)));
//        wordCloud.setFontScalar(new LinearFontScalar(12, 60));
        
        wordCloud.setFontScalar(new SqrtFontScalar(10, 60));
        wordCloud.setBackgroundColor(new Color(0x3D3D3D));
//        wordCloud.setFontScalar(new LogFontScalar(15,45));
        Font font = new Font("楷体", Font.PLAIN, 10);
        wordCloud.setKumoFont(new KumoFont(font));
        wordCloud.build(wordFrequencies);
        wordCloud.writeToFile("D:\\wechat/word" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")) + ".png");
    }
    
    public static void dataset() throws IOException {
        final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
        frequencyAnalyzer.setWordFrequenciesToReturn(300);
        frequencyAnalyzer.setMinWordLength(5);
//        frequencyAnalyzer.setStopWords(loadStopWords());
        
        final List<WordFrequency> wordFrequencies = frequencyAnalyzer.load("text/new_york_positive.txt");
        final List<WordFrequency> wordFrequencies2 = frequencyAnalyzer.load("text/new_york_negative.txt");
        final Dimension dimension = new Dimension(600, 386);
        final LayeredWordCloud layeredWordCloud = new LayeredWordCloud(2, dimension, CollisionMode.PIXEL_PERFECT);
        
        layeredWordCloud.setPadding(0, 1);
        layeredWordCloud.setPadding(1, 1);

//        layeredWordCloud.setFontOptionsptions(0, new KumoFont("LICENSE PLATE", FontWeight.BOLD));
//        layeredWordCloud.setFontOptionsptions(1, new KumoFont("Comic Sans MS", FontWeight.BOLD));
        
        layeredWordCloud.setBackground(0, new PixelBoundryBackground("backgrounds/cloud_bg.bmp"));
        layeredWordCloud.setBackground(1, new PixelBoundryBackground("backgrounds/cloud_fg.bmp"));
        
        layeredWordCloud.setColorPalette(0, new ColorPalette(new Color(0xABEDFF), new Color(0x82E4FF), new Color(0x55D6FA)));
        layeredWordCloud.setColorPalette(1, new ColorPalette(new Color(0xFFFFFF), new Color(0xDCDDDE), new Color(0xCCCCCC)));
        
        layeredWordCloud.setFontScalar(0, new SqrtFontScalar(10, 40));
        layeredWordCloud.setFontScalar(1, new SqrtFontScalar(10, 40));
        
        layeredWordCloud.build(0, wordFrequencies);
        layeredWordCloud.build(1, wordFrequencies2);
        layeredWordCloud.writeToFile("kumo-core/output/layered_word_cloud.png");
    }
}
