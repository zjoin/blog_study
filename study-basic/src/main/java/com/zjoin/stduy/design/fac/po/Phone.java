package com.zjoin.stduy.design.fac.po;

import lombok.Data;

/**
 * @Author: dgjava.com
 * @Date: 2020/5/1 5:41 PM
 * 顶层手机对象 phone
 */
@Data
public class Phone {
    
    protected String brand;
}