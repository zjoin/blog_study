package com.zjoin.stduy.design.fac.factory;

import com.zjoin.stduy.design.fac.product.Product;
import lombok.extern.slf4j.Slf4j;

/**
 * 类名 PhoneFactory
 * 描述 具体工厂-手机工厂
 *
 * @Author: dgjava.com
 * @Date: 2020/5/1 5:41 PM
 */
@Slf4j
public class PhoneFactory implements Factory {
    
    @Override
    public <T extends Product> T factory(Class<T> c) {
        Product product = null;
        try {
            product = (Product) Class.forName(c.getName()).newInstance();
        } catch (Exception e) {
            log.error("获取产品实例异常。", e);
        }
        return (T) product;
    }
}
