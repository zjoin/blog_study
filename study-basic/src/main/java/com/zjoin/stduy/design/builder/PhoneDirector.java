package com.zjoin.stduy.design.builder;

import com.zjoin.stduy.design.builder.build.HuaweiBuilder;
import com.zjoin.stduy.design.builder.build.IPhoneBuilder;
import com.zjoin.stduy.design.builder.build.PhoneBuilder;
import com.zjoin.stduy.design.builder.po.Huawei;
import com.zjoin.stduy.design.builder.po.IPhone;

/**
 * 类名 PhoneDirector
 * 描述 产品建造指挥者
 *
 * @Author dgjava.com
 * @Date 2020/5/6 18:26
 * @Version 1.0
 **/
public class PhoneDirector {
    PhoneBuilder builder;
    
    public Huawei builderHuawei() {
        builder = new HuaweiBuilder();
        builder.buildCameras();
        builder.buildCpu();
        builder.buildScreen();
        builder.buildMemory();
        builder.buildNetwork();
        builder.buildMano();
        return (Huawei) builder.build();
    }
    
    public IPhone builderIphone() {
        builder = new IPhoneBuilder();
        builder.buildCameras();
        builder.buildCpu();
        builder.buildScreen();
        builder.buildMemory();
        builder.buildNetwork();
        builder.buildSensor();
        return (IPhone) builder.build();
    }
}
