package com.zjoin.stduy.design.fac.product;

import com.zjoin.stduy.design.fac.po.Phone;

/**
 * 类名 Product
 * 描述 抽象产品
 *
 * @Author dgjava.com
 * @Date 2020/5/6 14:09
 * @Version 1.0
 **/
public interface Product {
    Phone createPhone();
    
    void doSomeThing();
    
}
