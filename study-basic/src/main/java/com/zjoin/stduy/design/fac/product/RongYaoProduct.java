package com.zjoin.stduy.design.fac.product;

import com.zjoin.stduy.design.fac.po.Phone;
import com.zjoin.stduy.design.fac.po.RongYao;
import lombok.extern.slf4j.Slf4j;

/**
 * 类名 RongYaoProduct
 * 描述 具体产品：生产荣耀手机
 *
 * @Author dgjava.com
 * @Date 2020/5/6 14:22
 * @Version 1.0
 **/
@Slf4j
public class RongYaoProduct implements Product {
    @Override
    public Phone createPhone() {
        return new RongYao();
    }
    
    @Override
    public void doSomeThing() {
        log.info("生产荣耀手机");
    }
}
