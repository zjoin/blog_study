package com.zjoin.stduy.design.fac;

import com.zjoin.stduy.design.fac.factory.Factory;
import com.zjoin.stduy.design.fac.factory.PhoneFactory;
import com.zjoin.stduy.design.fac.po.Phone;
import com.zjoin.stduy.design.fac.product.HuaWeiProduct;
import com.zjoin.stduy.design.fac.product.Product;

/**
 * 类名 FactoryDeom
 * 描述
 *
 * @Author yangxw
 * @Date 2020/5/6 14:29
 * @Version 1.0
 **/
public class FactoryDeom {
    
public static void main(String[] args) {
    Factory factory = new PhoneFactory();
    Product product = factory.factory(HuaWeiProduct.class);
    
    Phone phone = product.createPhone();
    System.out.println(phone.getBrand());
    product.doSomeThing();
}
}
