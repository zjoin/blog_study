package com.zjoin.stduy.design.builder;

import com.zjoin.stduy.design.builder.po.Phone;

/**
 * 类名 BuilderDemo
 * 描述
 *
 * @Author yangxw
 * @Date 2020/5/6 18:32
 * @Version 1.0
 **/
public class BuilderDemo {
    public static void main(String[] args) {
        PhoneDirector director = new PhoneDirector();
        
        Phone huawei = director.builderHuawei();
        System.out.println("----- 华为手机 -----");
        System.out.println(huawei);
        System.out.println();
        System.out.println("----- IPhone手机 -----");
        Phone iphone = director.builderIphone();
        System.out.println(iphone);
    }
}
