package com.zjoin.stduy.design.builder.po;

import lombok.Data;

/**
 * 类名 IPhone
 * 描述 苹果手机
 *
 * @Author dgjava.com
 * @Date 2020/5/6 18:04
 * @Version 1.0
 **/
@Data
public class IPhone extends Phone {
    private String sensor;
    public IPhone() {
        this.brand = "苹果";
        this.os = "IOS";
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("品牌= ").append(this.brand).append("\n");
        sb.append("系统= ").append(this.os).append("\n");
        sb.append("内存= ").append(this.memory).append("\n");
        sb.append("处理器= ").append(this.cpu).append("\n");
        sb.append("摄像头= ").append(this.cameras).append("\n");
        sb.append("屏幕= ").append(this.screen).append("\n");
        sb.append("传感器= ").append(this.sensor).append("\n");
        sb.append("网络= ").append(this.network);
        
        return sb.toString();
    }
}
