package com.zjoin.stduy.design.builder.po;

import lombok.Data;

/**
 * 类名 Phone
 * 描述 手机产品类
 *
 * @Author dgjava.com
 * @Date 2020/5/6 17:46
 * @Version 1.0
 **/
@Data
public abstract class Phone {
    protected String brand;
    protected String screen;
    protected String os;
    protected String cpu;
    protected String memory;
    protected String cameras;
    protected String network;
}
