package com.zjoin.stduy.design.fac.factory;

import com.zjoin.stduy.design.fac.product.Product;

/**
 * 类名 Factory
 * 描述 抽象工厂
 *
 * @Author dgjava.com
 * @Date 2020/5/6 14:09
 * @Version 1.0
 **/
public interface Factory {
    /**
     * 抽象工厂方法，对外暴露的工厂方法
     * 根据该方法获取具体工厂再生产具体产品
     * @param c
     * @param <T>
     * @return
     */
    <T extends Product> T factory(Class<T> c);
}
