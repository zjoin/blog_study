package com.zjoin.stduy.design.fac.product;

import com.zjoin.stduy.design.fac.po.HuaWei;
import com.zjoin.stduy.design.fac.po.Phone;
import lombok.extern.slf4j.Slf4j;

/**
 * 类名 HuaWeiProduct
 * 描述 具体产品：生产华为手机
 *
 * @Author dgjava.com
 * @Date 2020/5/6 14:21
 * @Version 1.0
 **/
@Slf4j
public class HuaWeiProduct implements Product{
    @Override
    public Phone createPhone() {
        return new HuaWei();
    }
    
    @Override
    public void doSomeThing() {
        log.info("生产华为手机");
    }
}
