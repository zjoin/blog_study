package com.zjoin.stduy.volatiles;

/**
 * 类名 MemoryUserDemo
 * 描述
 *
 * @Author yangxw
 * @Date 2020/4/30 17:01
 * @Version 1.0
 **/
public class MemoryUserDemo {
    
    public static void main(String[] args) {
        int size =64;
        int[][] arr= new int[size][size];
        
        
        for (int i =0;i<size;i++){
            for (int j = 0;j<size;j++){
                arr[i][j]=i+j;
            }
        }
    
    
        long l1 = System.currentTimeMillis();
        for (int i =0;i<size;i++){
            for (int j = 0;j<size;j++){
                System.out.print(arr[i][j]);
            }
        }
        System.out.println();
        long l2 = System.currentTimeMillis();
        for (int i =0;i<size;i++){
            for (int j = 0;j<size;j++){
                System.out.print(arr[j][i]);
            }
        }
        long l3 = System.currentTimeMillis();
        System.out.println();
    
        System.out.println("------");
        System.out.println(l2-l1);
        System.out.println(l3-l2);
    }
}
