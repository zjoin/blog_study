package com.zjoin.study.controller;

import cn.hutool.json.JSONUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Author: 地瓜
 * @Date: 2020/4/4 12:08 AM
 */
@RestController
@RequestMapping("/")
public class HttpController {

    @PostMapping("/index")
    public Object index(){
        PO po = new PO();
        po.setNow(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日HH时mm分ss秒")));
        return JSONUtil.toJsonPrettyStr(po);
    }
}
