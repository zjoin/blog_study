package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 整数反转
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/21 14:50
 * @Version 1.0
 **/
public class 整数反转 {
    
    public static int reverse(int x) {
        int c = Integer.MAX_VALUE;
        c++;
        if (x == 0) {
            return x;
        }
        boolean pos = true;
        if (x < 0) {
            pos = false;
            x = Math.abs(x);
        }
        char[] s = String.valueOf(x).toCharArray();
        if (s.length > 1) {
            int j = s.length - 1;
            for (int i = 0; i < s.length / 2; ) {
                char r = s[i];
                s[i] = s[j];
                s[j] = r;
                i++;
                j--;
            }
        }
        try {
            
            x = Integer.parseInt(String.valueOf(s));
            return pos ? x : -x;
        } catch (Exception e) {
        
        }
        return -1;
    }
    
    public static void main(String[] args) {
        System.out.println(reverse(345));
    }
}
