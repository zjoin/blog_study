package com.zjoin.study.arithmetic.基础;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 类名 Human
 * 描述
 *
 * @Author yangxw
 * @Date 2020/4/3 15:21
 * @Version 1.0
 **/
@Data
@Builder
@ToString
public class Human {
    private String name;
    private String sex;
    private Integer age;
    private BigDecimal weight;
    
    
}
