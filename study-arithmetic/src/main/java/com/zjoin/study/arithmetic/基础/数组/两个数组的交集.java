package com.zjoin.study.arithmetic.基础.数组;

import com.zjoin.study.arithmetic.基础.ArraysUtil;

import java.util.Arrays;

/**
 * 类名 两个数组的交集
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/19 17:29
 * @Version 1.0
 **/
public class 两个数组的交集 {
    public static int[] intersect(int[] nums1, int[] nums2) {
        
        int[] res = {};
        if (nums1.length <= 0 || nums2.length <=0) {
            return res;
        }
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int a = nums1.length;
        int b = nums2.length;
        if (nums1[0] > nums2[b - 1] || nums1[a - 1] < nums2[0]) {
            return res;
        }
        res = new int[Math.min(a, b)];
        // 1,4,6
        // 2,3,5
        int x = 0, z = 0;
        int y = 0;
        for (int i = x; i < a; i++) {
            for (int j = z; j < b; j++) {
                if (nums1[i] == nums2[j]) {
                  
                    res[y] = nums1[i];
                    z = j + 1;
                    y++;
                    System.out.println("nums1[" + i + "] == nums2[" + j + "]");
                    break;
                }
            }
        }
        if (y>0) {
            return Arrays.copyOfRange(res, 0, y);
        }else {
            return new int[]{};
        }
    }
    
    public static void main(String[] args) {
        int[] a = {};
        int[] b = {};
        int[] c = intersect(a, b);
        
        
        ArraysUtil.print(c);
    }
}
