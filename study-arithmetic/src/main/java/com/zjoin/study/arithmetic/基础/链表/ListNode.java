package com.zjoin.study.arithmetic.基础.链表;

/**
 * 类名 ListNode
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/24 17:19
 * @Version 1.0
 **/

public class ListNode {
    int val;
    ListNode next;
    
    ListNode(int x) {
        val = x;
    }
}
