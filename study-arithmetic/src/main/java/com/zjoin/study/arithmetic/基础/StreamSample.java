package com.zjoin.study.arithmetic.基础;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 类名 StreamSample
 * 描述
 *
 * @Author yangxw
 * @Date 2020/4/3 15:19
 * @Version 1.0
 **/
@Slf4j
public class StreamSample {
    public static List<Human> data() {
        String[] names = {"黄征", "王菲", "李建", "那英", "周杰伦"};
        List<String> ns = Arrays.asList(names);
        List<Human> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(Human.builder().age(i * 7 + 3)
                    .name(ns.get(i))
                    .sex(i % 2 == 0 ? "male" : "female")
                    .weight(new BigDecimal(i * 4 + 3.28 * (i + 1) + "").setScale(2, BigDecimal.ROUND_HALF_DOWN))
                    .build());
        }
        System.out.println("原始数据：");
        list.forEach(human -> {
            System.out.println(human.toString());
        });
        return list;
    }
    
    
    public static void groupBy(List<Human> list) {
        Map<String, List<Human>> map = list.stream().collect(Collectors.groupingBy(Human::getSex));
        System.out.println("\n性别分组：");
        map.forEach((k, v) -> {
            System.out.println(k + " → " + JSONUtil.toJsonPrettyStr(v.stream().map(Human::getName).collect(Collectors.toList())));
        });
    }
    
    public static void filter(List<Human> list) {
        List<Human> males = list.stream().filter(h -> h.getSex().equals("male")).collect(Collectors.toList());
        System.out.println("所有男性：");
        males.forEach(human -> {
            System.out.println(human.toString());
        });
    }
    
    public static void sourByName(List<Human> list) {
        List<Human> males = list.stream().sorted(Comparator.comparing(Human::getName).reversed()).collect(Collectors.toList());
        System.out.println("排姓名序后：");
        males.forEach(human -> {
            System.out.println(human.toString());
        });
    }
    
    
    public static void main(String[] args) {
        List<Human> list = data();
        sum1(list);
        sum2(list);
        average1(list);
        average2(list);
        max(list);
    }
    
    public static void sum1(List<Human> list) {
        Integer ages = list.stream().mapToInt(Human::getAge).sum();
        System.out.println("\n所有人年龄求和：" + ages);
    }
    
    public static void sum2(List<Human> list) {
        BigDecimal weights = list.stream().map(Human::getWeight).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println("\n所有人体重求和：" + weights);
    }
    
    public static void average1(List<Human> list) {
        Double aveAge = list.stream().mapToInt(Human::getAge).average().getAsDouble();
        System.out.println("\n所有人年龄求平均：" + aveAge);
    }
    
    public static void average2(List<Human> list) {
        BigDecimal weights = list.stream().map(Human::getWeight).reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(list.size())).setScale(2,BigDecimal.ROUND_HALF_DOWN);
        System.out.println("\n所有人体重求平均：" + weights);
    }
    
    public static void max(List<Human> list) {
    BigDecimal w = BigDecimal.ZERO;
    for (Human h:list){
        w=w.add(h.getWeight());
    }
        
        Human h = list.stream().max(Comparator.comparing(Human::getAge)).get();
        System.out.println("\n年龄最大的：" + h.toString());
        
       
    }
}
