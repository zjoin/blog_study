package com.zjoin.study.arithmetic.基础.数组;

import com.zjoin.study.arithmetic.基础.ArraysUtil;

/**
 * 类名 旋转图像
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/21 11:12
 * @Version 1.0
 **/
public class 旋转图像 {
    /**
     * 给定一个 n × n 的二维矩阵表示一个图像。
     * <p>
     * 将图像顺时针旋转 90 度。
     * <p>
     * 给定 matrix =
     * [
     * [1,2,3,a],
     * [4,5,6,b],
     * [7,8,9,c],
     * [d,e,f,g],
     * ],
     * <p>
     * 00 -> 03
     * 01 -> 13
     * 02 -> 23
     * 03 -> 33
     * <p>
     * 10 -> 02
     * 11 -> 12
     * 12 -> 22
     * 13 -> 32
     * <p>
     * 20 -> 01
     * 21 -> 11
     * 22 -> 21
     * 23 -> 31
     * <p>
     * 30 -> 00
     * 31 -> 10
     * 32 -> 20
     * 33 -> 30
     * <p>
     * ij -> j(l-i)
     * <p>
     * <p>
     * 原地旋转输入矩阵，使其变为:
     * [
     * [d,7,4,1],
     * [e,8,5,2],
     * [f,9,6,3],
     * [g,c,b,a]
     * ]
     */
    
    public static void rotate(int[][] matrix) {
        int m = matrix.length;
        int l = m - 1;
        for (int i = 0; i < l; i++) {
            for (int j = i; j < l-i; j++) {
                int x = matrix[i][j];
                matrix[i][j] = matrix[l - j][i];
                matrix[l - j][i] = matrix[l - i][l - j];
                matrix[l - i][l - j] = matrix[j][l - i];
                matrix[j][l - i] = x;
//                System.out.print(i+""+j+"= ");
//                System.out.print((l - j) + "" + i + " -> " + (i) + "" + j + " ");
//                System.out.print((l - i) + "" + (l-j) + " -> " + (l - j) + "" + i + " ");
//                System.out.print(j + "" + (l-j) + " -> " + (l - i) + "" + (l-j)+ " ");
//                System.out.print(i + "" + j + " -> " + (j) + "" + (l-i)+ " ");
//                System.out.println();
            
            }
        }
    }
    
    public static void main(String[] args) {
        int[][] mm = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };
        print(mm);
        rotate(mm);
        
        
    }
    
    private static void print(int[][] mm) {
        for (int i = 0; i < mm.length; i++) {
            ArraysUtil.print(mm[i]);
            System.out.println();
        }
        System.out.println();
    }
    
}
