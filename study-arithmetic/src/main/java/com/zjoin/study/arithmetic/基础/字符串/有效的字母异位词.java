package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 有效的字母异位词
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/21 16:52
 * @Version 1.0
 **/
public class 有效的字母异位词 {
    public static boolean isAnagram(String s, String t) {
        
        if (s.length() != t.length()) {
            return false;
        }
        int[] s1 = new int[26];
        int[] t1 = new int[26];
        for (int i = 0; i < s.length(); i++) {
            s1[s.charAt(i) - 97]++;
            t1[t.charAt(i) - 97]++;
            
        }
        for (int i = 0; i < 26; i++) {
            if (s1[i] != t1[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        
        System.out.println(isAnagram("hqbqo", "lsnma"));
    }
}
