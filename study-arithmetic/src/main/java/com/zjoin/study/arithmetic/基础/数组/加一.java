package com.zjoin.study.arithmetic.基础.数组;

import com.zjoin.study.arithmetic.基础.ArraysUtil;

/**
 * 类名 加一
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/20 15:09
 * @Version 1.0
 **/
public class 加一 {
    /**
     * 定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
     * <p>
     * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
     * <p>
     * 你可以假设除了整数 0 之外，这个整数不会以零开头。
     * 输入: [9,9]
     * 输出: [1,0,0]
     */
    public static int[] plusOne(int[] digits) {
        int l = digits.length;
        int s = digits[l - 1];
        if (s == 9) {
            for (int i = digits.length - 1; i >= 0; ) {
                if (i - 1 >= 0 && digits[i - 1] == 9) {
                    digits[i] = 0;
                    i--;
                } else if (i - 1 >= 0 && digits[i - 1] < 9) {
                    digits[i] = 0;
                    digits[i - 1] = digits[i - 1] + 1;
                    break;
                } else {
                    //进位 数组边长
                    int[] res = new int[digits.length + 1];
                    res[0] = 1;
                    return res;
                }
            }
            return digits;
        } else {
            digits[l - 1] = s + 1;
            return digits;
        }
    }
    
    public static void main(String[] args) {
        int[] s = {9,9, 9};
        ArraysUtil.print(plusOne(s));
    }
}
