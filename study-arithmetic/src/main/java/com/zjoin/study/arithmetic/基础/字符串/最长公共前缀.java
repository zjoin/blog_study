package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 最长公共前缀
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/24 16:47
 * @Version 1.0
 **/
public class 最长公共前缀 {
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length==0){
            return "";
        }
        if (strs.length==1){
            return strs[0];
        }
        int x = strs[0].length();
        StringBuilder sb = new StringBuilder();
        boolean flag = true;
        for (int j = 0; j < x; j++) {
            char y = strs[0].charAt(j);
            for (int i = 1; i < strs.length; ) {
                x = Math.min(strs[i].length(), x);
                if (x==0){
                    return "";
                }
                if (y == strs[i].charAt(j)) {
                    i++;
                } else {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                break;
            } else {
                sb.append(y);
            }
        }
        return sb.toString();
    }
}
