package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 反转字符串
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/21 14:35
 * @Version 1.0
 **/
public class 反转字符串 {
    /**
     * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 char[] 的形式给出。
     */
    
    /**
     * 输入：["h","e","l","l","o"]
     * 输出：["o","l","l","e","h"]
     */
    
    public static void reverseString(char[] s) {
        if (s.length<2){
            return;
        }
        int j = s.length - 1;
        for (int i = 0; i < s.length / 2; ) {
            
            char r = s[i];
            s[i] = s[j];
            s[j] = r;
            i++;
            j--;
        }
    }
    
    public static void main(String[] args) {
        char[] a = {'1', '2'};
        reverseString(a);
        for (char s:a){
            System.out.print(s);
        }
    }
    
}
