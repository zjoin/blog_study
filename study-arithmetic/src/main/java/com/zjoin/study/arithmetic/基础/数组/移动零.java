package com.zjoin.study.arithmetic.基础.数组;

import com.zjoin.study.arithmetic.基础.ArraysUtil;

/**
 * 类名 移动零
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/20 15:58
 * @Version 1.0
 **/
public class 移动零 {
    /**
     * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
     * <p>
     * 输入: [0,1,0,3,12]
     * 输出: [1,3,12,0,0]
     */
    
    public static void moveZeroes(int[] nums) {
        if (nums.length <= 1) {
            return;
        }
        int l = nums.length;
        int j = -1;
        for (int i = 0; i < l; i++) {
            if (nums[i] != 0 && j >= 0) {
                //前面有0 替换
                nums[j] = nums[i];
                nums[i] = 0;
                j++;
            } else if (nums[i] == 0 && j < 0) {
                j = i;
            }
        }
        ArraysUtil.print(nums);
    }
    
    public static void main(String[] args) {
        int[] nums = {0, 1, 1, 0, 0, 3, 0, 5, 0, 0, 6, 7, 8, 0};
        moveZeroes(nums);
    }
}
