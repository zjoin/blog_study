package com.zjoin.study.arithmetic.基础.数组;

import cn.hutool.json.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类名 有效的数独
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/20 16:54
 * @Version 1.0
 **/
public class 有效的数独 {
    /**
     * {
     * {"5","3",".",".","7",".",".",".","."},
     * {"6",".",".","1","9","5",".",".","."},
     * {".","9","8",".",".",".",".","6","."},
     * {"8",".",".",".","6",".",".",".","3"},
     * {"4",".",".","8",".","3",".",".","1"},
     * {"7",".",".",".","2",".",".",".","6"},
     * {".","6",".",".",".",".","2","8","."},
     * {".",".",".","4","1","9",".",".","5"},
     * {".",".",".",".","8",".",".","7","9"}
     * }
     */
    
    public static boolean isValidSudoku(char[][] board) {
       /* String[][] board = {
                {"5", "3", ".", ".", "7", ".", ".", ".", "."},
                {"6", ".", ".", "1", "9", "5", ".", ".", "."},
                {".", "9", "8", ".", ".", ".", ".", "6", "."},
                {"8", ".", ".", ".", "6", ".", ".", ".", "3"},
                {"4", ".", ".", "8", ".", "3", ".", ".", "1"},
                {"7", ".", ".", ".", "2", ".", ".", ".", "6"},
                {".", "6", ".", ".", ".", ".", "2", "8", "."},
                {".", ".", ".", "4", "1", "9", ".", ".", "5"},
                {".", ".", ".", ".", "8", ".", ".", "7", "9"}
            
        };*/
        Map<Integer, Map<Integer, List<Character>>> h = new HashMap<>();
        Map<Integer, Map<Integer, List<Character>>> v = new HashMap<>();
        Map<Integer, Map<Integer, List<Character>>> k = new HashMap<>();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j]!='.') {
                    if (!h.containsKey(i)) {
                        h.put(i, new HashMap<>());
                    }
                    if (h.get(i).size() > 0) {
                        if (h.get(i).get(h.get(i).size() - 1).contains(board[i][j])) {
                            return false;
                        } else {
                            h.get(i).get(h.get(i).size() - 1).add(board[i][j]);
                            h.get(i).put(h.get(i).size(), h.get(i).get(h.get(i).size() - 1));
                        }
                    } else {
                        List<Character> ll = new ArrayList<>();
                        ll.add(board[i][j]);
                        Map<Integer, List<Character>> mm = new HashMap<>();
                        mm.put(0, ll);
                        h.put(i, mm);
                    }
                    if (!v.containsKey(j)) {
                        
                        v.put(j, new HashMap<>());
                    }
                    
                    if (v.get(j).size() > 0) {
                        if (v.get(j).get(v.get(j).size() - 1).contains(board[i][j])) {
                            return false;
                        } else {
                            v.get(j).get(v.get(j).size() - 1).add(board[i][j]);
                            v.get(j).put(v.get(j).size(), v.get(j).get(v.get(j).size() - 1));
                        }
                    } else {
                        List<Character> ll = new ArrayList<>();
                        ll.add(board[i][j]);
                        Map<Integer, List<Character>> mm = new HashMap<>();
                        mm.put(0, ll);
                        v.put(j, mm);
                    }
                 
                        //00 03 33 36 ……
                        int p = 10 * (i / 3) + j / 3;
                        if (!k.containsKey(p)) {
                            k.put(p, new HashMap<>());
                        }
                        if (k.get(p).size() > 0) {
                            if (k.get(p).get(k.get(p).size() - 1).contains(board[i][j])) {
                                return false;
                            } else {
                                k.get(p).get(k.get(p).size() - 1).add(board[i][j]);
                                k.get(p).put(k.get(p).size(), k.get(p).get(k.get(p).size() - 1));
                            }
                        } else {
                            List<Character> ll = new ArrayList<>();
                            ll.add(board[i][j]);
                            Map<Integer, List<Character>> mm = new HashMap<>();
                            mm.put(0, ll);
                            k.put(p, mm);
                        }
                }
                
            }
        }
        System.out.println(JSONUtil.toJsonPrettyStr(k));
        return true;
    }
    
    public static void main(String[] args) {
        char[][] board = {
                {'.','.','.'  ,'.','5','.'  ,'.','1','.'},
                {'.','4','.'  ,'3','.','.'  ,'.','.','.'},
                {'.','.','.'  ,'.','.','3'  ,'.','.','1'},
                
                {'8','.','.'  ,'.','.','.'  ,'.','2','.'},
                {'.','.','2'  ,'.','7','.'  ,'.','.','.'},
                {'.','1','5'  ,'.','.','.'  ,'.','.','.'},
                
                {'.','.','.'  ,'.','.','2'  ,'.','.','.'},
                {'.','2','.'  ,'9','.','.'  ,'.','.','.'},
                {'.','.','4'  ,'.','.','.'  ,'.','.','.'}};
        System.out.println(isValidSudoku(board));
    }
}
