package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 字符串转换整数
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/24 11:17
 * @Version 1.0
 **/
public class 字符串转换整数 {
    
    
    public static int myAtoi(String str) {
        if (str.length()==0){
            return 0;
        }
        str=str.trim();
        char[] chars = str.toCharArray();
        int n = chars.length;
        int idx = 0;
        boolean negative = false;
        if (chars[idx] == '-') {
            //遇到负号
            negative = true;
            idx++;
        } else if (chars[idx] == '+') {
            // 遇到正号
            idx++;
        } else if (!Character.isDigit(chars[idx])) {
            // 其他符号
            return 0;
        }
        int ans = 0;
        while (idx < n && Character.isDigit(chars[idx])) {
            int digit = chars[idx] - '0';
            if (ans > (Integer.MAX_VALUE - digit) / 10) {
                return negative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            ans = ans * 10 + digit;
            idx++;
        }
        return negative ? -ans : ans;
    }
    
    public static void main(String[] args) {
        System.out.println(myAtoi("-91283472332"));
    }
}
