package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 实现strStr
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/24 14:29
 * @Version 1.0
 **/
public class 实现strStr {
    public static int strStr(String haystack, String needle) {
        if (haystack.length() == 0 && needle.length() == 0) {
            return 0;
        }
        if (needle.length() > haystack.length()) {
            return -1;
        }
        return haystack.indexOf(needle);
    }
    
    public static void main(String[] args) {
        System.out.println(strStr("", ""));
    }
}
