package com.zjoin.study.arithmetic.基础.数组;

/**
 * 类名 买卖股票的最佳时机
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/14 14:30
 * @Version 1.0
 **/
public class 买卖股票的最佳时机 {
    
    /**
     * 给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
     *
     * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
     */
    
    /**
     * 输入: [7,1,5,8,5,3,6,4]
     * 输出: 7
     * 解释: 在第 2 天（股票价格 = 1）的时候买入，在第 3 天（股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。
     *      随后，在第 4 天（股票价格 = 3）的时候买入，在第 5 天（股票价格 = 6）的时候卖出, 这笔交易所能获得利润 = 6-3 = 3 。
     */
    
    public static int maxProfit(int[] prices) {
        int i = 0;
        int amount = 0;
        for (int j = i + 1; j < prices.length; j++) {
           
            if (prices[i] < prices[j]) {
                amount += prices[j] - prices[i];
            }
            i = j;
        }
        return amount;
    }
    
    public static void main(String[] args) {
        int[] nums = {7, 1, 5, 3, 6, 4};
        System.out.println(maxProfit(nums));
    }
}
