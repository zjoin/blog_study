package com.zjoin.study.arithmetic.基础.数组;

import java.util.HashSet;
import java.util.Set;

/**
 * 类名 存在重复元素
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/19 16:45
 * @Version 1.0
 **/
public class 存在重复元素 {
    /**
     * 给定一个整数数组，判断是否存在重复元素。
     * <p>
     * 如果任意一值在数组中出现至少两次，
     * 函数返回 true 。
     * 如果数组中每个元素都不相同，则返回 false 。
     */
    public static boolean containsDuplicate(int[] nums) {
        if (nums == null || nums.length < 2) {
            return false;
        }
        Set<Integer> set = new HashSet<>(nums.length );
        for (int i = 0; i < nums.length; i++) {
           boolean f = set.add(nums[i]);
           if(!f){
               return true;
           }
        }
        return  false;
    }
    
    public static void main(String[] args) {
     
       
        int[] nums = {1, 2, 3, 4, 5, 4,5,6};
        System.out.println(containsDuplicate(nums));
    }
}
