package com.zjoin.study.arithmetic.基础.字符串;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 类名 字符串中的第一个唯一字符
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/21 15:26
 * @Version 1.0
 **/
public class 字符串中的第一个唯一字符 {
    public static int firstUniqChar(String s) {
        int x = -1;
        
        //字符、下标，
        
        if (s != null) {
            s = s.trim();
            if (!s.equals("")) {
                if (s.length() == 1) {
                    return 0;
                }
                Set<String> sl = new HashSet<>();
                //字符、下标，
                Map<String, Integer> map = new HashMap<>();
                String[] ss = s.split("");
                for (int i = ss.length - 1; i >= 0; i--) {
                    if (sl.add(ss[i])) {
                        map.put(ss[i], i);
                    } else {
                        if (map.containsKey(ss[i])) {
                            map.remove(ss[i]);
                        }
                    }
                }
                if (map.size() > 0) {
                    x = Integer.MAX_VALUE;
                    for (String c : map.keySet()) {
                        x = Math.min(map.get(c), x);
                    }
                }
            }
        }
        return x;
    }
    
    public static void main(String[] args) {
        System.out.println(firstUniqChar(""));
    }
}
