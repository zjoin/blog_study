package com.zjoin.study.arithmetic.基础.字符串;

/**
 * 类名 验证回文串
 * 描述
 *
 * @Author xwyang
 * @Date 2020/8/24 10:35
 * @Version 1.0
 **/
public class 验证回文串 {
    public static boolean isPalindrome(String s) {
        int j = s.length() - 1;
        int i=0;
        for (; i <=j; ) {
            int a = s.charAt(i) - 0;
            int b = s.charAt(j) - 0;
            if (a >= 65 && a <= 90) {
                a += 32;
            }
            if (b >= 65 && b <= 90) {
                b += 32;
            }
            if (a > 122 || a < 97 && a > 57 || a < 48) {
                i++;
                continue;
            }
            if (b > 122 || b < 97 && b > 57 || b < 48) {
                j--;
                continue;
            }
            System.out.println(s.charAt(i)+" "+s.charAt(j));
            if (a != b) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
    
    public static void main(String[] args) {
        System.out.println(isPalindrome(",,,,,,,,,,,,acva"));
        String s = "+09";
        for (int i = 0; i < 3; i++) {
            System.out.println(s.charAt(i) - 0);
        }
    }
    
    
}
